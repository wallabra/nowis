import { Color } from "./style";
export interface XY {
    x: number;
    y: number;
}
export interface Bounds {
    left: number;
    top: number;
    right: number;
    bottom: number;
}
export interface BoundFlags {
    left: boolean;
    top: boolean;
    right: boolean;
    bottom: boolean;
}
export declare function boundFlags(s: string): BoundFlags;
export declare type PartialBounds = Partial<Bounds>;
export interface XYFlags {
    onX?: boolean;
    onY?: boolean;
}
export declare class Vec {
    coord: XY;
    constructor(coord: XY);
    x(val?: number): number;
    y(val?: number): number;
    sqsize(): number;
    size(): number;
    add(other: Vec): Vec;
    sub(other: Vec): Vec;
    mul(other: Vec | number): Vec;
    div(other: Vec | number): Vec;
    dot(other: Vec): number;
    unit(): Vec;
    xy(): XY;
}
export declare function xy(x: number, y: number): XY;
export declare function xyvec(x: number, y: number): Vec;
export declare function rgb(r: number, g: number, b: number): Color;
export declare function intersect(...boundaries: Bounds[]): Bounds;
export declare function union(...boundaries: Bounds[]): Bounds;
export declare function sizeBounds(size: XY, offs?: XY): Bounds;
export declare function boundsPos(bounds: Bounds): XY;
export declare function boundsSize(bounds: Bounds): XY;
export declare function moveBounds(bounds: Bounds, offs: XY): Bounds;
export declare function ltrb(l: number, t: number, r: number, b: number): Bounds;
export declare function printBounds(bo: Bounds): void;
//# sourceMappingURL=common.d.ts.map