"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function boundFlags(s) {
    return {
        left: s.indexOf('w') != -1 || s.indexOf('l') != -1,
        top: s.indexOf('n') != -1 || s.indexOf('t') != -1,
        right: s.indexOf('e') != -1 || s.indexOf('r') != -1,
        bottom: s.indexOf('s') != -1 || s.indexOf('b') != -1,
    };
}
exports.boundFlags = boundFlags;
var Vec = /** @class */ (function () {
    function Vec(coord) {
        this.coord = coord;
    }
    Vec.prototype.x = function (val) {
        if (val != null)
            this.coord.x = val;
        return this.coord.x;
    };
    Vec.prototype.y = function (val) {
        if (val != null)
            this.coord.y = val;
        return this.coord.y;
    };
    Vec.prototype.sqsize = function () {
        return Math.pow(this.x(), 2) + Math.pow(this.y(), 2);
    };
    Vec.prototype.size = function () {
        return Math.sqrt(this.sqsize());
    };
    Vec.prototype.add = function (other) {
        return new Vec({
            x: this.x() + other.x(),
            y: this.y() + other.y(),
        });
    };
    Vec.prototype.sub = function (other) {
        return new Vec({
            x: this.x() - other.x(),
            y: this.y() - other.y(),
        });
    };
    Vec.prototype.mul = function (other) {
        if (other instanceof Vec)
            return new Vec({
                x: this.x() * other.x(),
                y: this.y() * other.y(),
            });
        return new Vec({
            x: this.x() * other,
            y: this.y() * other,
        });
    };
    Vec.prototype.div = function (other) {
        if (other instanceof Vec)
            return new Vec({
                x: this.x() / other.x(),
                y: this.y() / other.y(),
            });
        return new Vec({
            x: this.x() / other,
            y: this.y() / other,
        });
    };
    Vec.prototype.dot = function (other) {
        return this.x() * other.x() + this.y() * other.y();
    };
    Vec.prototype.unit = function () {
        return this.div(this.size());
    };
    Vec.prototype.xy = function () {
        return {
            x: this.x(),
            y: this.y(),
        };
    };
    return Vec;
}());
exports.Vec = Vec;
function xy(x, y) {
    return { x: x, y: y };
}
exports.xy = xy;
function xyvec(x, y) {
    return new Vec(xy(x, y));
}
exports.xyvec = xyvec;
function rgb(r, g, b) {
    return { r: r, g: g, b: b };
}
exports.rgb = rgb;
function intersect() {
    var boundaries = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        boundaries[_i] = arguments[_i];
    }
    var res = {
        left: -Infinity,
        top: -Infinity,
        right: Infinity,
        bottom: Infinity
    };
    boundaries.forEach(function (b) {
        if (b.left > res.left)
            res.left = b.left;
        if (b.top > res.top)
            res.top = b.top;
        if (b.right < res.right)
            res.right = b.right;
        if (b.bottom < res.bottom)
            res.bottom = b.bottom;
    });
    return res;
}
exports.intersect = intersect;
function union() {
    var boundaries = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        boundaries[_i] = arguments[_i];
    }
    var res = {
        left: Infinity,
        top: Infinity,
        right: -Infinity,
        bottom: -Infinity
    };
    boundaries.forEach(function (b) {
        if (b.left < res.left)
            res.left = b.left;
        if (b.top < res.top)
            res.top = b.top;
        if (b.right > res.right)
            res.right = b.right;
        if (b.bottom > res.bottom)
            res.bottom = b.bottom;
    });
    return res;
}
exports.union = union;
function sizeBounds(size, offs) {
    if (offs === void 0) { offs = xy(0, 0); }
    return {
        left: offs.x,
        top: offs.y,
        right: size.x + offs.x,
        bottom: size.y + offs.y,
    };
}
exports.sizeBounds = sizeBounds;
function boundsPos(bounds) {
    return {
        x: bounds.left,
        y: bounds.top
    };
}
exports.boundsPos = boundsPos;
function boundsSize(bounds) {
    return {
        x: bounds.right - bounds.left,
        y: bounds.bottom - bounds.top
    };
}
exports.boundsSize = boundsSize;
function moveBounds(bounds, offs) {
    return {
        left: bounds.left + offs.x,
        top: bounds.top + offs.y,
        right: bounds.right + offs.x,
        bottom: bounds.bottom + offs.y,
    };
}
exports.moveBounds = moveBounds;
function ltrb(l, t, r, b) {
    return {
        left: l,
        top: t,
        right: r,
        bottom: b
    };
}
exports.ltrb = ltrb;
function printBounds(bo) {
    var t = '' + bo.top;
    var l = '' + bo.left;
    var r = '' + bo.right;
    var b = '' + bo.bottom;
    console.log(' '.repeat(l.length) + "  " + t + "\n" + ' '.repeat(l.length) + " +" + '-'.repeat(Math.max(t.length - 2, 1)) + "+\n" + l + " |" + ' '.repeat(Math.max(t.length - 2, 1)) + "| " + r + "\n" + ' '.repeat(l.length) + " +" + '-'.repeat(Math.max(t.length - 2, 1)) + "+\n" + ' '.repeat(l.length) + "  " + b);
}
exports.printBounds = printBounds;
//# sourceMappingURL=common.js.map