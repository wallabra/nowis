"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var widget_1 = require("../widget");
var window_1 = require("../window");
exports.default = {
    data: function () {
        return { mouseInside: false };
    },
    listeners: {
        'mouse move': function mouse_mouseMove(origin, newPos, offset) {
            var _this = this;
            if (this.$widget === origin) {
                if (!this.mouseInside) {
                    if (this.$widget instanceof widget_1.Widget && this.$widget.window) {
                        this.$widget.window.emitDownFromHere('mouse exit');
                    }
                    else if (this.$widget instanceof window_1.WindowWidget && this.$widget.system) {
                        this.$widget.system.windows.forEach(function (c) {
                            if (c !== _this.$widget && c.data.mouseInside) {
                                c.data.mouseInside = false;
                                c.emitDownFromHere('mouse exit');
                            }
                        });
                    }
                    this.mouseInside = true;
                    this.$widget.emitDownFromHere('mouse enter');
                }
            }
        }
    }
};
//# sourceMappingURL=mouse.js.map