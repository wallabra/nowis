import { WidgetHandler, BaseWidget, MethodThis } from '../widget';
import { Bounds } from '../common';
export default function (windowLimits: (this: MethodThis, origin: BaseWidget) => Bounds): WidgetHandler;
//# sourceMappingURL=window-limits.d.ts.map