"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var events_1 = require("events");
var common_1 = require("./common");
var widget_1 = require("./widget");
var window_1 = require("./window");
var LayoutCell = /** @class */ (function () {
    function LayoutCell(grid, content, layoutData) {
        this.grid = grid;
        this.content = content;
        this.layoutData = layoutData;
    }
    LayoutCell.prototype.getBounds = function () {
        if (!this.bounds)
            this.grid.update();
        return JSON.parse(JSON.stringify(this.bounds));
    };
    LayoutCell.prototype.getSize = function () {
        if (!this.bounds)
            this.grid.update();
        return common_1.boundsSize(this.bounds);
    };
    LayoutCell.prototype.isColumn = function (x) {
        return this.layoutData.pos.x <= x && x < this.layoutData.pos.x + this.layoutData.size.x;
    };
    LayoutCell.prototype.isRow = function (y) {
        return this.layoutData.pos.y <= y && y < this.layoutData.pos.y + this.layoutData.size.y;
    };
    LayoutCell.prototype.minSizeX = function () {
        return this.content.minSizeX();
    };
    LayoutCell.prototype.minSizeY = function () {
        return this.content.minSizeY();
    };
    LayoutCell.prototype.getCellBounds = function () {
        return {
            left: this.layoutData.pos.x,
            top: this.layoutData.pos.y,
            right: this.layoutData.pos.x + this.layoutData.size.x,
            bottom: this.layoutData.pos.y + this.layoutData.size.y,
        };
    };
    return LayoutCell;
}());
exports.LayoutCell = LayoutCell;
var Grid = /** @class */ (function (_super) {
    __extends(Grid, _super);
    function Grid() {
        var _this = _super.call(this) || this;
        _this.tiles = new Map();
        _this.cells = new Set();
        _this.cellBounds = {
            left: Infinity,
            top: Infinity,
            right: -Infinity,
            bottom: -Infinity
        };
        _this.pixelBounds = {
            left: Infinity,
            top: Infinity,
            right: -Infinity,
            bottom: -Infinity
        };
        _this.columns = new Set();
        _this.rows = new Set();
        _this.columnSizes = new Map();
        _this.rowSizes = new Map();
        return _this;
    }
    Grid.prototype.hashPos = function (x, y) {
        return '' + x + '|' + y;
    };
    Grid.prototype.cellAt = function (x, y) {
        return this.tiles.get(this.hashPos(x, y));
    };
    Grid.prototype.allWidth = function () {
        var padding = 0;
        if (this instanceof window_1.WindowWidget) {
            if (this.data.padding)
                padding = this.data.padding.left + this.data.padding.right;
        }
        if (this instanceof widget_1.Widget) {
            if (this.data.padding)
                padding = this.data.padding.left + this.data.padding.right;
        }
        if (this instanceof window_1.WindowWidget)
            return this.data.size.x - padding;
        else if (this instanceof widget_1.Widget) {
            return this.cell.getSize().x - padding;
        }
        return 0;
    };
    Grid.prototype.allHeight = function () {
        var padding = 0;
        if (this instanceof window_1.WindowWidget) {
            if (this.data.padding)
                padding = this.data.padding.top + this.data.padding.bottom;
        }
        if (this instanceof widget_1.Widget) {
            if (this.data.padding)
                padding = this.data.padding.top + this.data.padding.bottom;
        }
        if (this instanceof window_1.WindowWidget) {
            return this.data.size.y - padding;
        }
        else if (this instanceof widget_1.Widget) {
            return this.cell.getSize().y - padding;
        }
        return 0;
    };
    Grid.prototype.columnSize = function (x, debug) {
        var _this = this;
        if (debug === void 0) { debug = false; }
        if (this.columnSizes.has(x))
            return this.columnSizes.get(x);
        var cc = Array.from(this.cells).filter(function (c) { return c.isColumn(x); });
        var res = 0;
        if (cc) {
            res = Math.max.apply(Math, __spreadArrays([0], cc.map(function (c) {
                var w = _this.allWidth();
                var d = _this.columns.size;
                var r;
                var rc = Array.from(_this.cells).filter(function (a) { return a.layoutData.pos.y >= c.layoutData.pos.y && a.layoutData.pos.y + a.layoutData.size.y <= c.layoutData.pos.y + c.layoutData.size.y; });
                if (c.content.data.layout.shrink)
                    r = c.minSizeX();
                else {
                    rc.forEach(function (a) {
                        if (a.content.data.layout.shrink && d > 1) {
                            w -= a.content.minSizeX();
                            d--;
                        }
                    });
                    r = Math.max(w / d, c.minSizeX());
                }
                return r;
            })));
        }
        this.columnSizes.set(x, res);
        return res;
    };
    Grid.prototype.columnMinSize = function (x, debug) {
        if (debug === void 0) { debug = false; }
        var cc = Array.from(this.cells).filter(function (c) { return c.isColumn(x); });
        var res = 0;
        if (cc) {
            res = Math.max.apply(Math, __spreadArrays([0], cc.map(function (c) {
                var r = c.minSizeX() / c.layoutData.size.x;
                return r;
            })));
        }
        return res;
    };
    Grid.prototype.rowSize = function (y) {
        var _this = this;
        if (this.rowSizes.has(y))
            return this.rowSizes.get(y);
        var cc = Array.from(this.cells).filter(function (c) { return c.isRow(y); });
        var res = 0;
        if (cc) {
            res = Math.max.apply(Math, __spreadArrays([0], cc.map(function (c) {
                var w = _this.allHeight();
                var d = _this.rows.size;
                var r;
                var rc = Array.from(_this.cells).filter(function (a) { return a.layoutData.pos.x >= c.layoutData.pos.x && a.layoutData.pos.x + a.layoutData.size.x <= c.layoutData.pos.x + c.layoutData.size.x; });
                if (c.content.data.layout.shrink)
                    r = c.minSizeY();
                else {
                    rc.forEach(function (a) {
                        if (a.content.data.layout.shrink && d > 1) {
                            w -= a.content.minSizeY();
                            d--;
                        }
                    });
                    r = Math.max(w / d, c.minSizeY());
                }
                return r;
            })));
        }
        this.rowSizes.set(y, res);
        return res;
    };
    Grid.prototype.rowMinSize = function (y) {
        var rc = Array.from(this.cells).filter(function (c) { return c.isRow(y); });
        var res = 0;
        if (rc) {
            res = Math.max.apply(Math, __spreadArrays([0, this.allHeight() / this.rows.size], rc.map(function (c) { return c.minSizeY() / c.layoutData.size.y; })));
        }
        return res;
    };
    Grid.prototype.addAsCell = function (widget) {
        if (widget.cell != null)
            return;
        var layout = widget.data.layout;
        var sticky = layout.sticky;
        var pos = layout.cellPos;
        var size = layout.cellSize;
        var cell = new LayoutCell(this, widget, {
            sticky: sticky,
            pos: pos,
            size: size
        });
        this.cells.add(cell);
        for (var x = pos.x; x < pos.x + size.x; x++) {
            this.columns.add(x);
            for (var y = pos.y; y < pos.y + size.y; y++) {
                this.tiles.set(this.hashPos(x, y), cell);
            }
        }
        for (var y = pos.y; y < pos.y + size.y; y++) {
            this.rows.add(y);
        }
        widget.cell = cell;
        this.cellBounds = common_1.union(this.cellBounds, cell.getCellBounds());
        this.update();
        this.pixelBounds = common_1.union(this.pixelBounds, cell.getBounds());
        return cell;
    };
    Grid.prototype.remove = function (cell) {
        var _this = this;
        this.tiles.forEach(function (c, key) {
            if (c === cell)
                _this.tiles.delete(key);
        });
        this.cells.delete(cell);
        this.update();
    };
    Grid.prototype.gridMinWidth = function () {
        var _this = this;
        return Array.from(this.columns).map(function (c) { return _this.columnMinSize(c); }).reduce(function (a, b) { return a + b; }, 0);
    };
    Grid.prototype.gridMinHeight = function () {
        var _this = this;
        return Array.from(this.rows).map(function (r) { return _this.rowMinSize(r); }).reduce(function (a, b) { return a + b; }, 0);
    };
    Grid.prototype.update = function () {
        this._update();
        this.cells.forEach(function (c) {
            c.content.update();
        });
    };
    Grid.prototype.padding = function () {
        return { left: 0, top: 0, right: 0, bottom: 0 };
    };
    Grid.prototype.calculateCellSizes = function () {
        var _this = this;
        this.columnSizes = new Map();
        this.rowSizes = new Map();
        var pad = this.padding();
        var x = pad.left;
        var y = pad.top;
        var cols = new Map();
        var rows = new Map();
        Array.from(this.columns).sort(function (a, b) { return a - b; }).forEach(function (col) {
            var sz = _this.columnSize(col);
            cols.set(col, {
                a: x,
                b: x += sz
            });
        });
        Array.from(this.rows).sort(function (a, b) { return a - b; }).forEach(function (row) {
            var sz = _this.rowSize(row);
            rows.set(row, {
                a: y,
                b: y += sz
            });
        });
        this.cells.forEach(function (c) {
            var x1 = c.layoutData.pos.x;
            var x2 = c.layoutData.pos.x + c.layoutData.size.x - 1;
            var y1 = c.layoutData.pos.y;
            var y2 = c.layoutData.pos.y + c.layoutData.size.y - 1;
            c.bounds = {
                left: cols.get(x1).a,
                top: rows.get(y1).a,
                right: cols.get(x2).b,
                bottom: rows.get(y2).b,
            };
        });
    };
    Grid.prototype._update = function () {
        this.calculateCellSizes();
        while (true) {
            if (!Array.from(this.cells).some(function (c) { return c.content.larger(); }))
                break;
            this.cells.forEach(function (c) {
                c.content.growCell();
            });
            this.calculateCellSizes();
        }
        this.cells.forEach(function (c) {
            c.content.cellResized();
        });
    };
    return Grid;
}(events_1.EventEmitter));
exports.Grid = Grid;
//# sourceMappingURL=layout.js.map