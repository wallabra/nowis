import { WidgetData } from './widget';
import { WindowData } from './window';
export interface ProtocolCreateWidgetPerformer {
    kind: 'create-widget';
    id: string;
    parentId: string;
    name: string | null;
    data: Partial<WidgetData>;
}
export interface ProtocolCreateWindowPerformer {
    kind: 'create-window';
    id: string;
    name: string | null;
    data: Partial<WindowData>;
}
export interface ProtocolDestroyPerformer {
    kind: 'destroy';
    id: string;
    reason: string | null;
}
export interface ProtocolAnnihilatePerformer {
    kind: 'annihilate';
    reason: string | null;
}
export interface ProtocolSetPerformer {
    kind: 'set';
    id: string;
    keys: string[];
    value: any;
}
export interface ProtocolEmitUpPerformer {
    kind: 'emit-up';
    id: string;
    event: string;
    args: any[];
}
export interface ProtocolEmitDownPerformer {
    kind: 'emit-down';
    id: string;
    event: string;
    args: any[];
}
export interface ProtocolEmitPerformer {
    kind: 'emit';
    id: string;
    event: string;
    args: any[];
}
export declare type ProtocolPerformer = (ProtocolCreateWidgetPerformer | ProtocolCreateWindowPerformer | ProtocolDestroyPerformer | ProtocolSetPerformer | ProtocolEmitUpPerformer | ProtocolEmitDownPerformer | ProtocolEmitPerformer | ProtocolAnnihilatePerformer);
export interface ProtocolListQuery {
    kind: 'list';
}
export interface ProtocolNameQuery {
    kind: 'name';
    id: string;
}
export interface ProtocolDataQuery {
    kind: 'data';
    id: string;
    keys: string[];
}
export declare type ProtocolQuery = (ProtocolNameQuery | ProtocolDataQuery);
export interface ProtocolQueryCall {
    id: string;
    type: 'query';
    call: ProtocolQuery;
}
export interface ProtocolPerformerCall {
    id: string;
    type: 'perform';
    call: ProtocolPerformer;
}
export declare type ProtocolCall = (ProtocolQueryCall | ProtocolPerformerCall);
export interface ProtocolStatus {
    type: number;
    id: string;
    reason?: string;
}
export interface ProtocolResponse {
    callId: string;
    status: ProtocolStatus;
    result?: any;
}
export interface ProtocolEvent {
    type: 'event';
    event: string;
    at: string;
    origin: string;
    args: any[];
}
export declare function makeResponse(this: ProtocolCall, status: ProtocolStatus, result?: any): {
    callId: string;
    status: ProtocolStatus;
    result: any;
};
//# sourceMappingURL=protocol.d.ts.map