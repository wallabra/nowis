import { Bounds, XY } from './common';
import { AreaStyle } from './style';
import { WindowSystem } from './winsystem';
import { BaseWidget } from './widget';
export interface WindowData {
    title?: string;
    pos: XY;
    size: XY;
    clip?: Bounds;
    style: AreaStyle;
    padding?: Bounds;
}
export interface WindowOptions {
    id?: string;
    name?: string;
    system?: WindowSystem;
    data?: any;
    activeSystems?: string[];
    components?: {
        [name: string]: any;
    };
}
export declare class WindowWidget extends BaseWidget {
    data: WindowData;
    serialize(system?: WindowSystem): {
        '$wt': string;
        '$wn': (name?: string | null | undefined) => string | null;
        '$wi': string;
        '$wv': any;
        '$wc': any;
        '$ws': string[];
    };
    static deserialize(sys: WindowSystem, data: any): WindowWidget;
    inheritStyle(): AreaStyle;
    destroy(reason?: string | null): void;
    static make(opt: WindowOptions): WindowWidget;
    constructor(parent?: BaseWidget, data?: any | null, id?: string, name?: string);
    getGlobalBounds(): {
        left: number;
        top: number;
        right: number;
        bottom: number;
    };
    padding(): Bounds;
}
//# sourceMappingURL=window.d.ts.map