"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var widget_1 = require("./widget");
var WindowWidget = /** @class */ (function (_super) {
    __extends(WindowWidget, _super);
    function WindowWidget(parent, data, id, name) {
        if (data === void 0) { data = null; }
        var _this = 
        // Windows can't have parents.
        _super.call(this) || this;
        _this.data = {
            style: {
                colors: {
                    bg: { r: 1, g: 1, b: 1 },
                    text: { r: 0, g: 0, b: 0 }
                },
                substyles: {
                    selected: {
                        bg: { r: 0, g: 0.5, b: 1 },
                        text: { r: 1, g: 1, b: 1 }
                    }
                },
                font: {
                    family: 'Verdana',
                    size: 12,
                    weight: 500,
                },
            },
            pos: {
                x: 0,
                y: 0,
            },
            size: {
                x: 1,
                y: 1,
            }
        };
        Object.assign(_this.data, data);
        if (id)
            _this.id = id;
        if (name)
            _this._name = name;
        return _this;
    }
    WindowWidget.prototype.serialize = function (system) {
        return {
            '$wt': 'window',
            '$wn': this.name,
            '$wi': this.id,
            '$wv': (system || this.system).prep(this.data),
            '$wc': (system || this.system).prep(this.components.entries()),
            '$ws': Array.from(this.activeSystems),
        };
    };
    WindowWidget.deserialize = function (sys, data) {
        return WindowWidget.make({
            data: sys.unprep(data.$wv),
            id: data.$wi,
            name: data.$wn,
            components: new Map(sys.unprep(data.$wc)),
            activeSystems: data.$ws
        });
    };
    WindowWidget.prototype.inheritStyle = function () {
        return this.data.style;
    };
    WindowWidget.prototype.destroy = function (reason) {
        if (reason === void 0) { reason = null; }
        _super.prototype.destroy.call(this, reason);
        if (this.system) {
            this.system.widgets.delete(this.id);
            this.system.windows.delete(this.id);
        }
    };
    WindowWidget.make = function (opt) {
        var res = new WindowWidget(undefined, {}, opt.id, opt.name || undefined);
        res.system = opt.system;
        if (opt.data) {
            if (opt.data.pos)
                res.data.pos = opt.data.pos;
            if (opt.data.size)
                res.data.size = opt.data.size;
            if (opt.data.title)
                res.data.title = opt.data.title;
            if (opt.data.padding)
                res.data.padding = opt.data.padding;
            if (opt.data.style)
                Object.assign(res.data.style, opt.data.style || {});
            if (opt.data.clip)
                Object.assign(res.data.clip, opt.data.clip || {});
        }
        if (opt.components)
            res.components = new Map(Object.entries(opt.components));
        if (opt.activeSystems)
            res.activeSystems = new Set(opt.activeSystems);
        return res;
    };
    // Layout Methods
    WindowWidget.prototype.getGlobalBounds = function () {
        var pad = this.padding();
        return {
            left: this.data.pos.x + pad.left,
            top: this.data.pos.y + pad.top,
            right: this.data.pos.x + this.data.size.x + pad.left + pad.right,
            bottom: this.data.pos.y + this.data.size.y + pad.top + pad.bottom,
        };
    };
    WindowWidget.prototype.padding = function () {
        if (this.data.padding) {
            return JSON.parse(JSON.stringify(this.data.padding));
        }
        return _super.prototype.padding.call(this);
    };
    return WindowWidget;
}(widget_1.BaseWidget));
exports.WindowWidget = WindowWidget;
//# sourceMappingURL=window.js.map