"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var window_1 = require("./window");
var widget_1 = require("./widget");
var style_1 = require("./style");
var common_1 = require("./common");
var Protocol = __importStar(require("./protocol"));
var events_1 = require("events");
var WindowSystem = /** @class */ (function (_super) {
    __extends(WindowSystem, _super);
    function WindowSystem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.windows = new Map();
        _this.widgets = new Map();
        _this.handlers = [];
        _this.windowHandlers = [];
        _this.widgetSystems = new Map();
        return _this;
    }
    WindowSystem.prototype.prep = function (a) {
        if (a instanceof common_1.Vec) {
            return {
                '$t': 'v',
                '$v': a.xy()
            };
        }
        else if (a instanceof widget_1.BaseWidget) {
            return {
                '$t': 'w',
                '$v': a.id
            };
        }
        else if (a instanceof style_1.ColorFiller) {
            return {
                '$t': 'c',
                '$v': a.col
            };
        }
        else if (a instanceof style_1.LinearGradient) {
            return {
                '$t': 'l',
                '$v': {
                    'o': a.origin,
                    't': a.toward,
                    's': a.start,
                    'p': a.stop,
                    'a': a.from,
                    'b': a.to,
                }
            };
        }
        else if (a instanceof style_1.RadialGradient) {
            return {
                '$t': 'r',
                '$v': {
                    'o': a.origin,
                    'r': a.radius,
                    's': a.start,
                    'p': a.stop,
                    'a': a.from,
                    'b': a.to,
                }
            };
        }
        else {
            return {
                '$t': '',
                '$v': this.rawPrep(a)
            };
        }
    };
    WindowSystem.prototype.rawPrep = function (a) {
        var _this = this;
        if (Array.isArray(a))
            return a.map(function (x) { return _this.prep(x); });
        else if (typeof a === 'object' && a != null) {
            var res_1 = {};
            Object.keys(a).forEach(function (k) {
                res_1[k] = _this.prep(a[k]);
            });
            return res_1;
        }
        else
            return a;
    };
    WindowSystem.prototype.unprep = function (a) {
        if (!a.$t) {
            return this.rawUnprep(a.$v);
        }
        else if (a.$t === 'v') {
            return new common_1.Vec(a.$v);
        }
        else if (a.$t === 'w') {
            return this.widgets.get(a.$v);
        }
        else if (a.$t === 'c') {
            return new style_1.ColorFiller(a.$v);
        }
        else if (a.$t === 'l') {
            return new style_1.LinearGradient(a.$v.o, a.$v.t, a.$v.a, a.$v.b, a.$v.s, a.$v.p);
        }
        else if (a.$t === 'r') {
            return new style_1.RadialGradient(a.$v.o, a.$v.r, a.$v.a, a.$v.b, a.$v.s, a.$v.p);
        }
    };
    WindowSystem.prototype.rawUnprep = function (a) {
        var _this = this;
        if (Array.isArray(a))
            return a.map(function (x) { return _this.unprep(a); });
        else if (typeof a === 'object') {
            var res_2 = {};
            Object.keys(a).forEach(function (k) {
                res_2[k] = _this.unprep(a[k]);
            });
            return res_2;
        }
        else
            return a;
    };
    WindowSystem.prototype.add = function (w) {
        var _this = this;
        if (w instanceof widget_1.HandlingSystem) {
            this.widgetSystems.set(w.name, w);
            return;
        }
        this.handlers.forEach(function (handler) {
            w.register(handler);
        });
        w.onAny(function (evt, at, origin) {
            var args = [];
            for (var _i = 3; _i < arguments.length; _i++) {
                args[_i - 3] = arguments[_i];
            }
            _this.emit.apply(_this, __spreadArrays(['widget event', evt, at, origin], args));
        });
        if (w instanceof window_1.WindowWidget) {
            this.windowHandlers.forEach(function (handler) {
                w.register(handler);
            });
            this.windows.set(w.id, w);
        }
        w.system = this;
        this.widgets.set(w.id, w);
        w.children.forEach(function (c) {
            _this.add(c);
        });
    };
    WindowSystem.prototype.apply = function (pcall) {
        var _a, _b, _c;
        var respond = Protocol.makeResponse.bind(pcall);
        switch (pcall.type) {
            case 'perform':
                {
                    var call = pcall.call;
                    switch (call.kind) {
                        case 'create-widget':
                            {
                                var w = this.widgets.get(call.parentId);
                                if (w != null && (w instanceof window_1.WindowWidget || w instanceof widget_1.Widget)) {
                                    var res = widget_1.Widget.make(w, {
                                        data: call.data || null,
                                        id: call.id,
                                        name: call.name || undefined
                                    });
                                    this.add(res);
                                    return respond({ type: 0, id: 'SUCCESS', reason: 'Widget created successfully.' }, res.serialize());
                                }
                                else
                                    return respond({ type: 2, id: 'PARENT-NOT-FOUND', reason: 'Parent not found.' });
                            }
                            ;
                        case 'create-window':
                            {
                                var res = window_1.WindowWidget.make({
                                    data: call.data || null,
                                    id: call.id,
                                    name: call.name || undefined
                                });
                                this.add(res);
                                return respond({ type: 0, id: 'SUCCESS', reason: 'Window created successfully.' }, res.serialize());
                            }
                            ;
                        case 'destroy':
                            {
                                if (this.widgets.has(call.id)) {
                                    this.widgets.get(call.id).destroy(call.reason);
                                    return respond({ type: 0, id: 'SUCCESS', reason: 'BaseWidget destroyed successfully.' });
                                }
                                return respond({ type: 1, id: 'WIDGET-NOT-FOUND', reason: 'BaseWidget not found.' });
                            }
                            ;
                        case 'annihilate':
                            {
                                var c_1 = call;
                                this.windows.forEach(function (w) {
                                    w.destroy(c_1.reason);
                                });
                                this.widgets.forEach(function (w) {
                                    w.destroy(c_1.reason);
                                });
                                return respond({ type: 0, id: 'SUCCESS', reason: 'All windows and widgets destroyed successfully.' });
                            }
                            ;
                        case 'emit':
                            {
                                if (this.widgets.has(call.id)) {
                                    (_a = this.widgets.get(call.id)).emitOutFromHere.apply(_a, __spreadArrays([call.event], call.args));
                                    return respond({ type: 0, id: 'SUCCESS', reason: 'Bidirectional event emitted successfully.' });
                                }
                                return respond({ type: 1, id: 'WIDGET-NOT-FOUND', reason: 'BaseWidget not found.' });
                            }
                            ;
                        case 'emit-up':
                            {
                                if (this.widgets.has(call.id)) {
                                    (_b = this.widgets.get(call.id)).emitUpFromHere.apply(_b, __spreadArrays([call.event], call.args));
                                    return respond({ type: 0, id: 'SUCCESS', reason: 'Upward event emitted successfully.' });
                                }
                                return respond({ type: 1, id: 'WIDGET-NOT-FOUND', reason: 'BaseWidget not found.' });
                            }
                            ;
                        case 'emit-down':
                            {
                                if (this.widgets.has(call.id)) {
                                    (_c = this.widgets.get(call.id)).emitDownFromHere.apply(_c, __spreadArrays([call.event], call.args));
                                    return respond({ type: 0, id: 'SUCCESS', reason: 'Downward event emitted successfully.' });
                                }
                                return respond({ type: 1, id: 'WIDGET-NOT-FOUND', reason: 'BaseWidget not found.' });
                            }
                            ;
                        case "set":
                            {
                                var widget = this.widgets.get(call.id);
                                if (widget !== undefined) {
                                    var keys = call.keys;
                                    keys.unshift('data');
                                    var res_3 = widget;
                                    keys.slice(0, -1).forEach(function (k, i) {
                                        if (!(k in res_3)) {
                                            return respond({ type: 3, id: 'KEY-NOT-FOUND', reason: "Key #" + (i - 1) + " (" + k + ") not found in this widget's data!" });
                                        }
                                        res_3 = res_3[k];
                                    });
                                    res_3[keys.slice(-1)[0]] = call.value;
                                    return respond({ type: 0, id: 'SUCCESS', reason: 'Widget value set successfully.' });
                                }
                                ;
                                return respond({ type: 1, id: 'WIDGET-NOT-FOUND', reason: 'BaseWidget not found.' });
                            }
                            ;
                        default:
                            {
                                return respond({ type: -1, id: 'INVALID-CALL', reason: "Invalid call: no such performer kind '" + call.kind + "'!" });
                            }
                            ;
                    }
                    ;
                }
                ;
            case "query":
                {
                    var call = pcall.call;
                    switch (call.kind) {
                        case "data":
                            {
                                var widget = this.widgets.get(call.id);
                                if (widget !== undefined) {
                                    var keys = call.keys;
                                    var res_4 = widget.data;
                                    keys.forEach(function (k, i) {
                                        if (!(k in res_4)) {
                                            return respond({ type: 3, id: 'KEY-NOT-FOUND', reason: "Key #" + i + " (" + k + ") not found in this widget's data!" });
                                        }
                                        res_4 = res_4[k];
                                    });
                                    return respond({ type: 0, id: 'SUCCESS', reason: 'Widget value retrieved successfully.' }, res_4);
                                }
                                ;
                                return respond({ type: 1, id: 'WIDGET-NOT-FOUND', reason: 'BaseWidget not found.' });
                            }
                            ;
                        case "name":
                            {
                                var widget = this.widgets.get(call.id);
                                if (widget !== undefined) {
                                    return respond({ type: 0, id: 'SUCCESS', reason: 'Widget name retrieved successfully.' }, widget.name());
                                }
                                ;
                                return respond({ type: 1, id: 'WIDGET-NOT-FOUND', reason: 'BaseWidget not found.' });
                            }
                            ;
                        default:
                            {
                                return respond({ type: -1, id: 'INVALID-CALL', reason: "Invalid call: no such query kind '" + call.kind + "'!" });
                            }
                            ;
                    }
                }
                ;
            default:
                {
                    return respond({ type: -2, id: 'INVALID', reason: "Invalid protocol call: '" + pcall.type + "' is not one of 'performer', 'query" });
                }
                ;
        }
    };
    WindowSystem.prototype.registerAllWidgets = function (handler) {
        this.widgets.forEach(function (w) {
            w.register(handler);
        });
        this.handlers.push(handler);
    };
    WindowSystem.prototype.registerAllWindows = function (handler) {
        this.windows.forEach(function (w) {
            w.register(handler);
        });
        this.windowHandlers.push(handler);
    };
    WindowSystem.prototype.touching = function (pos) {
        var _this = this;
        return Array.from(this.widgets.values()).filter(function (w) { return _this.isTouching(pos, w); });
    };
    WindowSystem.prototype.isTouching = function (pos, widget) {
        var gb = widget.getGlobalBounds();
        return (pos.x >= gb.left &&
            pos.x <= gb.right &&
            pos.y >= gb.top &&
            pos.y <= gb.bottom);
    };
    WindowSystem.prototype.emitAt = function (pos, evt) {
        var _a;
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var deepest = null;
        var depth = 0;
        this.touching(pos).forEach(function (w) {
            var d = w.depth();
            if (!deepest || d > depth)
                return (deepest = w, depth = d);
        });
        if (deepest)
            (_a = deepest).emitOutFromHere.apply(_a, __spreadArrays([evt], args));
    };
    return WindowSystem;
}(events_1.EventEmitter));
exports.WindowSystem = WindowSystem;
//# sourceMappingURL=winsystem.js.map