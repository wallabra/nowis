import { WidgetHandler, BaseWidget, Widget } from '../widget';
import { WindowWidget } from '../window';
import { Vec } from '../common';



export default {
    data: function() {
        return { 
            dragging: false,
            draggable: true,
        }
    },
    
    listeners: {
        'mouse down': function drag_mouseDown(origin: BaseWidget) {
            if (this.draggable && this.$widget === origin && origin instanceof WindowWidget) this.dragging = true;
        },

        'mouse up': function drag_mouseUp(origin: BaseWidget) {
            if (this.$widget === origin && origin instanceof WindowWidget)
                this.dragging = false;
        },

        'mouse exit': function drag_mouseUp(origin: BaseWidget) {
            if (this.$widget === origin && origin instanceof WindowWidget)
                this.dragging = false;
        },
        
        'mouse move': function drag_mouseMove(origin: BaseWidget, newPos: Vec, offset: Vec) {
            if (this.dragging && this.$widget === origin) {
                if (this.$widget instanceof WindowWidget) {
                    this.pos = new Vec(this.pos).add(offset).xy();
                    
                    this.$widget.emitDownFromHere('window drag', offset);
                }
            }
        }
    }
} as WidgetHandler;