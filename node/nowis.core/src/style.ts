import { XY, XYFlags, Vec } from './common';
import { WidgetLayout } from './widget';



export interface Font {
    family: string,
    size:   number,
    weight: number,
}



export abstract class Filler {
    abstract colorAt(relPos: Vec): Color;
}


export interface Color {
    r: number;
    g: number;
    b: number;
}


export class ColorFiller extends Filler {
    constructor(public col: Color) {
        super();
    }

    colorAt(relPos: Vec) {
        return this.col;
    }
}


export abstract class Gradient extends Filler {
    constructor(public from: Color, public to: Color, public start: number = 0, public stop: number = 1) {
        super();
    }

    interpColor(alpha: number): Color {
        alpha = this.start + alpha * (this.stop - this.start);

        return {
            r: (this.to.r - this.from.r) * alpha + this.from.r,
            g: (this.to.g - this.from.g) * alpha + this.from.g,
            b: (this.to.b - this.from.b) * alpha + this.from.b,
        };
    }
}


export class LinearGradient extends Gradient {
    constructor(public origin: XY, public toward: XY, public from: Color, public to: Color, public start: number = 0, public stop: number = 1) {
        super(from, to, start, stop);
    }

    colorAt(relPos: Vec) {
        let orig = new Vec(this.origin);
        let dir = new Vec(this.toward).sub(orig);
        let dsq = dir.sqsize();
        let offs = relPos.sub(orig);
        let dot = dir.dot(offs) / dsq;

        return this.interpColor(dot);
    }
}


export class RadialGradient extends Gradient {
    constructor(public origin: XY, public radius: number, public from: Color, public to: Color, public start: number = 0, public stop: number = 1) {
        super(from, to, start, stop);
    }

    colorAt(relPos: Vec) {
        let offs = relPos.sub(new Vec(this.origin));
        return this.interpColor(Math.min(1, Math.max(0, offs.size() / this.radius)));
    }
}


export interface ColorStyle {
    bg?:        Color | Filler;
    text?:      Color | Filler;
}


export interface AreaStyle {
    colors: ColorStyle;
    font: Font;

    substyles: {
        active?:    ColorStyle;
        selected?:  ColorStyle;
        hover?:     ColorStyle;
    };
}


export interface Style {

}